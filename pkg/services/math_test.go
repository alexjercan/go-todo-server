package services_test

import (
	"testing"

	"github.com/alexjercan/go-todo-server/pkg/services"
)

func TestAdd(t *testing.T) {

	got := services.Add(4, 6)
	want := 10

	if got != want {
		t.Errorf("got %q, wanted %q", got, want)
	}
}

func TestSubtract(t *testing.T) {

	got := services.Subtract(4, 6)
	want := -2

	if got != want {
		t.Errorf("got %q, wanted %q", got, want)
	}
}
